#/bin/bash

OUTPUT_FILE=$1
CURR_DIR=`pwd`

echo "Running Python Bandit - https://github.com/PyCQA/bandit"

echo '' > $OUTPUT_FILE
bandit -r $CURR_DIR >> $OUTPUT_FILE

echo "[SUCCESS] Python Bandit ran successfully"