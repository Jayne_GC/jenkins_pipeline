#/bin/bash
set -e

OUTPUT_FILE=$1
TARGET_CODE=`echo $2 | cut -d'/' -f5 | cut -d'.' -f1`
CURR_DIR=`pwd`
TARGET_DIRECTORY="${CURR_DIR}/vulnerability_reports/${TARGET_CODE}"
NEW_FILENAME="report_${TARGET_CODE}.txt"

echo "Publishing Report"

# Clone reports reposiroty
rm -rf $CURR_DIR/vulnerability_reports
git clone https://Jayne_GC@bitbucket.org/Jayne_GC/vulnerability_reports.git

# Create app filder if doesn't exist
if [ ! -d "${TARGET_DIRECTORY}" ]; then
  mkdir $TARGET_DIRECTORY
fi
cd $TARGET_DIRECTORY

# Copy report to repository and publish
cp $CURR_DIR/$OUTPUT_FILE $TARGET_DIRECTORY/$NEW_FILENAME
git add $NEW_FILENAME
git commit -am "Updating Report for ${TARGET_CODE}"
git push

cd $CURR_DIR
echo "[SUCCESS] Report $NEW_FILENAME published successfully"
echo "REPORT: https://bitbucket.org/Jayne_GC/vulnerability_reports/raw/master/${TARGET_CODE}/${NEW_FILENAME}"