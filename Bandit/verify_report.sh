#/bin/bash

OUTPUT_FILE=$1
CURR_DIR=`pwd`

echo "Verifying Report"

RESULTS_SEV=`cat $OUTPUT_FILE | grep -A 14 "Code scanned:" | grep -A 5 "severity" | grep "High:"`

# If any High severity found, fail the execution
HIGH_SEV_ZERO=`echo $RESULTS_SEV | grep "High: 0.0"`
HIGH_SEV_NUMBER=`echo $RESULTS_SEV | grep "High:" | cut -d':' -f2`

if [ -z "$HIGH_SEV_ZERO" ];then
  echo "    [ERROR] There are $HIGH_SEV_NUMBER High Severity issues! Aborting...";
  exit 1
else
  echo "    [SUCCESS] There are no High Severity issues!"
fi

# Add more checks
# ...
# ...
# ...

echo "[SUCCESS] The report has been verified!"