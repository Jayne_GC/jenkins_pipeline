#/bin/bash
set -e

OUTPUT_FILE=$1

echo "Running Python Taint"

echo '' > $OUTPUT_FILE

for filename in $(find . -type f -name *.py); do 
	echo $filename >> $OUTPUT_FILE
	python3 -m pyt $filename >> $OUTPUT_FILE
done

echo "[SUCCESS] Python Taint ran successfully"